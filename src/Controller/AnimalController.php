<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Animal;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class AnimalController extends AbstractController
{
    /**
     * @Route("/animal", name="animal")
     */
    public function new(Request $request)
    {

        $animal = new Animal();

        $form = $this->createFormBuilder($animal)
        ->add('rasa', TextType::class)
        ->add('culoare', TextType::class)
        ->add('save', SubmitType::class, ['label' => 'Create Animal'])
        ->getForm();

        $form->handleRequest($request);
        $succes = 0;

        if ($form->isSubmitted()) {
            $animal = $form->getData();
            $succes = 1;
            $enitityManager = $this->getDoctrine()->getManager();
            $enitityManager->persist($animal);
            $enitityManager->flush();
        } else {
            $succes = 2;
        }

        return $this->render('animal/index.html.twig', [
            'form' => $form->createView(),
            'succes' => $succes,
        ]); 
    }
}
