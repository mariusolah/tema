<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnimalRepository")
 */
class Animal
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rasa;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $culoare;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRasa(): ?string
    {
        return $this->rasa;
    }

    public function setRasa(string $rasa): self
    {
        $this->rasa = $rasa;

        return $this;
    }

    public function getCuloare(): ?string
    {
        return $this->culoare;
    }

    public function setCuloare(string $culoare): self
    {
        $this->culoare = $culoare;

        return $this;
    }
}
